all: hello.o
	gcc obj/hello.o src/main.c -o exec/hello

hello.o: src/hello.c
	gcc -c src/hello.c -o obj/hello.o

clean:
	rm -vf obj/* exec/*
	
